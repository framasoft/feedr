#!/usr/bin/env python3
""" Feedr, get RSS feed and tweet each entry
"""

import argparse
import sqlite3
import time
from typing import Union

import feedparser  # type: ignore
import tweepy  # type: ignore
import yaml

with open('feedr.yml', 'r', encoding='utf-8') as file:
    config = yaml.safe_load(file)
    file.close()

if 'database' in config:
    DATABASE = config['database']
else:
    DATABASE = 'database/rss_entries.db'

FEEDS = config['feeds']
CONSUMER_KEY = config['api']['key']
CONSUMER_SECRET = config['api']['secret']
ACCESS_TOKEN = config['access']['token']
ACCESS_TOKEN_SECRET = config['access']['secret']

# Define the net max length of the text portion of a tweet
TWEET_MAX_LENGTH = 280
TWEET_URL_LENGTH = 22
TWEET_NET_LENGTH = TWEET_MAX_LENGTH - TWEET_URL_LENGTH


def parse_and_tweet_entry(entry, hashtag: Union[str, None] = None):
    """ Check if the entry has already been tweeted, else tweet it

    Keyword arguments:
    entry   -- an RSS or ATOM entry
    hashtag -- string, hashtag to add to the tweet
    """
    cursor.execute('SELECT * FROM RSSContent WHERE url=?', (entry.link,))
    if not cursor.fetchall():
        data = (entry.link, entry.title,
                time.strftime("%Y-%m-%d %H:%M:%S", entry.updated_parsed))
        cursor.execute(
                'INSERT INTO RSSContent (`url`, `title`, `dateAdded`) \
                        VALUES (?,?,?)',
                data)

        hashtag_length = len(hashtag) if hashtag is not None else 0
        body_length = TWEET_NET_LENGTH - hashtag_length - 2

        body = entry.title[:body_length]
        url = entry.link

        if not args.just_print:
            client.create_tweet(text=f'{body}\n{url}\n{hashtag}',
                                user_auth=True)

        print(f"{time.strftime('%c')} - {body} {url} {hashtag}")
        conn.commit()


if __name__ == '__main__':
    # Handle options
    parser = argparse.ArgumentParser()
    parser.add_argument('-j', '--just-print',
                        dest='just_print',
                        help='Don’t tweet, just print the tweet that \
                                would have been created',
                        action='store_true')
    args = parser.parse_args()

    client = tweepy.Client(
        consumer_key=CONSUMER_KEY,
        consumer_secret=CONSUMER_SECRET,
        access_token=ACCESS_TOKEN,
        access_token_secret=ACCESS_TOKEN_SECRET
    )

    conn = sqlite3.connect(DATABASE)
    conn.row_factory = sqlite3.Row
    cursor = conn.cursor()
    cursor.execute(
            'CREATE TABLE IF NOT EXISTS RSSContent \
                    (`url`, `title`, `dateAdded`)')

    for feed in FEEDS:
        parsed_feed = feedparser.parse(feed['url'])

        for item in list(reversed(parsed_feed.entries)):
            parse_and_tweet_entry(item, feed['hashtag'])

    conn.close()
