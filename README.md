# feedr: RSS Bot for Twitter!

**NB**: this is a fork of <https://www.github.com/housed/feedr> to make it work with Python3.

feedr will read RSS feeds that you specify and then it will determine if there's new content since last checked.
If there is, then it will publish a post on your Twitter account on your behalf for each new content update.

As is, feedr will add the following information to a tweet:
* Title of the new content entry
* URL to the new content entry
* Relevant hashtag(s)

## Required Packages

* Python3
* Tweepy
* feedparser
* PyYAML

On Debian :

```bash
apt install python3-tweepy \
            python3-feedparser \
            python3-yaml
```

## Getting Started is easy

### Setup a Twitter app

Navigate to <https://apps.twitter.com/> and create a new Twitter App.
Configure the app in the following ways:

1. Enable Read and Write permissions.
2. Generate a Bearer Token in the "Keys and tokens" tab

### Configure Feedr

Copy the configuration template:
```bash
cp feedr.yml.template feedr.yml
```

Here’s the content of the configuration template:
```
---
database: database/rss_entries.test.db
feeds:
  - url: https://example.org/rss
    hashtag: '#FooBar'
api:
  key: foobar
  secret: bazqux
access:
  token: quuxcorge
  secret: graultgarply
```

- `database`: optional, default is `database/rss_entries.test.db`, where to create the database
- `feeds`: mandatory, array of feeds to fetch:
  - `url`: mandatory, the feed URL
  - `hashtag`: option, no default, the hashtag added to each tweet
- `api`: mandatory. Put your API key and secret in it
- `access`: mandatory. Put your access token and secret in it

### Use it!

For the first use, we suggest you to use the `--just-print` option to populate the database without flooding your twitter account.
```bash
./feedr.py --just-print
```

After that, you can use it without the option:
```bash
./feedr.py
```

You should put it in a cron job so that it runs periodically.

## License

As the original work, this fork is licensed under the terms of the MIT License.
See [LICENSE](LICENSE) file.
